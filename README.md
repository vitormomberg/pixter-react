# Teste de desenvolvimento Front End

[![N|Solid](https://pixter.com.br/wp-content/uploads/2018/06/logo-black.png)](http://pixter.com.br)

A proposta do teste é:

- Criar um feed _responsivo_ de **qualquer** usuário do Instagram
- Exibir informações do usuário escolhido
- Exibir as medias enviadas pelo usuário
- Exibir os comentários de cada media enviada
- Exibir modal assim que clicado em uma imagem

### Pontuaremos:

- Estrutura de projeto
- Genericidade de código (componentes reutilizáveis)
- Patterns
- Organização de CSS
- Responsividade

### Requisitos:

- Desenvolver em Javascript, nativo ou utilizando qualquer framework da linguagem. (ReactJS pontuará mais)
- Usar Promise.all em algum momento da aplicação
- Scroll infinito no feed

### Entrega:

- Faça um _**fork**_ deste repositório
- Crie uma branch com seu nome
- Desenvolva sua aplicação
- Crie um pull request quando finalizar
- Envie-nos um e-mail confirmando a finalização
